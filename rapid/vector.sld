;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;> Vector library compatible with SRFI 133.

(define-library (rapid vector)
  (export vector-unfold vector-unfold-right vector-reverse-copy 
          vector-concatenate vector-append-subvectors
	  vector-empty? vector=
	  vector-fold vector-fold-right vector-map!
	  vector-count vector-cumulate
	  vector-index vector-index-right vector-skip vector-skip-right 
	  vector-binary-search vector-any vector-every vector-partition
	  vector-swap! vector-reverse! 
	  vector-reverse-copy! vector-unfold! vector-unfold-right!
	  reverse-vector->list reverse-list->vector
	  vector-copy vector-append vector-map vector-for-each
	  vector-fill! vector-copy!
	  vector->list list->vector vector->string string->vector)
  (import (scheme base)
	  (scheme cxr)
	  (rapid receive))
  (include "vector.scm"))
